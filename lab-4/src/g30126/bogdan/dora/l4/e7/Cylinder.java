package g30126.bogdan.dora.l4.e7;

import g30126.bogdan.dora.l4.e3.Circle;

public class Cylinder extends Circle{
	private double height=1.0;
	public Cylinder() {
		// TODO Auto-generated constructor stub
	}
	
	public Cylinder(double radius)
	{
		super(radius);
		
	}
	public Cylinder(double radius,double height)
	{
		super(radius);
		this.height=height;
	}
	public double GetHeight() {
		return height;
	}
	public double GetArea() {
		super.GetArea();
		return Math.PI*super.GetRadius()*super.GetRadius();
	}
	public double GetVolume() {
		return GetHeight()*GetArea();
	}
	
}