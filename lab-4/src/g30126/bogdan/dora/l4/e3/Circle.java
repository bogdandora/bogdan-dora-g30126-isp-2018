package g30126.bogdan.dora.l4.e3;

public class Circle {

		private double radius;
		public String color;
		Circle(){
			// TODO Auto-generated constructor stub
			radius=1.0;
			color="red";
		}
		public Circle(double radius)
		{
			this.radius=radius;
		}
		public Circle(String color)
		{
			this.color=color;
		}
		public double GetRadius(){
			return radius;
		}
		public double GetArea(){
			return Math.PI*radius*radius;
		}

	}
