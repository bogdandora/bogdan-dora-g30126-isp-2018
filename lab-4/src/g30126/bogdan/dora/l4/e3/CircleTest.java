package g30126.bogdan.dora.l4.e3;

import static org.junit.Assert.*;
import org.junit.Test;

public class CircleTest {

	@Test
	public void testGetArea() {
		Circle c=new Circle();
		double area=c.GetArea();
		double a=Math.PI*1*1;
		assertEquals(area,a,0.01);
	}
}

