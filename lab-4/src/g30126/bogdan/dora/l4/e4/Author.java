package g30126.bogdan.dora.l4.e4;

public class Author {
	private String name;
	private String email;
	private char gender;
	
	public Author(String name,String email,char gender){
		this.name=name;
		this.email=email;
		this.gender=gender;
	}
	 public String getName(){
		 return name;
	 }
	 public String getEmail(){
		 return email;
	 }
	 public void setEmail(String email){
		 this.email=email;
	 }
	 public void setGender(char gender){
		 this.gender=gender;
	 }
	 public char getGender(){
		 return gender;
	 }
	 public String ToString(){
			return name+"("+gender+")"+" at "+email;
		}
}