package g30126.bogdan.dora.l3.e3;

import becker.robots.*;

public class ex3
{
   public static void main(String[] args)
   {  

   	City prague = new City();

      Robot karel = new Robot(prague, 6, 1, Direction.NORTH);
 
      karel.move();
      karel.move();
      karel.move();
      karel.move();
      karel.move();
      karel.turnLeft();	
      karel.turnLeft();
      karel.move();
      karel.move();
      karel.move();
      karel.move();
      karel.move();
   }
}