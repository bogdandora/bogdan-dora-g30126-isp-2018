package g30126.bogdan.dora.l9.e2;

import java.awt.*;
import javax.swing.*;

class TestButtonPanel {

    public static void main(String [] args)
    {
        JFrame buttonFrame = new JFrame("Button Panel");
        ButtonPanel panel = new ButtonPanel();

        buttonFrame.add(panel);
        buttonFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        buttonFrame.setSize(400,200);
        buttonFrame.setVisible(true);
    }
}
