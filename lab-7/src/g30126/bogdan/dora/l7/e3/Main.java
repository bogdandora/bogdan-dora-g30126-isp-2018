package g30126.bogdan.dora.l7.e3;

import java.util.ArrayList;
import java.util.TreeSet;

import g30126.bogdan.dora.l7.e1.BankAccount;
import g30126.bogdan.dora.l7.e3.*;

public class Main {

	public Main() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		TreeSet<BankAccount> bankAccounts=new TreeSet<BankAccount>();
		Bank bank=new Bank(bankAccounts);
		bank.addAccounts("xyz", 50);
		bank.addAccounts("abc", 500);
		bank.addAccounts("and", 1000);
		bank.addAccounts("or", 5000);
		bank.printAccounts();
		bank.printAccount(40, 100);
		System.out.println(bank.getAllAccount());

	}
}