package g30126.bogdan.dora.l7.e4;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.*;
import java.util.*;

public class Main {

	public Main() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) throws Exception{
		
		HashMap<Word, Definition>hMap=new HashMap<>();
		Dictionary dictionary=new Dictionary(hMap);
		char answer;
		String linie,explic;
		BufferedReader fluxIn=new BufferedReader(new InputStreamReader(System.in));
		do {
			System.out.println("Meniu:");
			System.out.println("1. Add word");
			System.out.println("2. Show definition");
			System.out.println("3. All words");
			System.out.println("4. All definitionsa");
			System.out.println("5. Exit");
			linie=fluxIn.readLine();
			answer=linie.charAt(0);
			switch (answer) {
			case 'a':case '1':
				System.out.println("Enter word:");
				linie=fluxIn.readLine();
				if(linie.length()>1) {
					System.out.println("Enter definition:");
					explic=fluxIn.readLine();
					dictionary.addWord(new Word(linie), new Definition(explic));
				}
				break;
			case 'b':case '2':
				System.out.println("Search word:");
				linie=fluxIn.readLine();
				if(linie.length()>1)
				{
					Definition explic1 = dictionary.getDefinition(new Word(linie));
					if (explic1 ==null) 
						System.out.println("Word not found!");
					else
						System.out.println("Definition:"+explic1.getDefinition());
				}
				break;
			case 'w':case'3':
			{
				dictionary.getAllWords();
				break;
			}
			case 'q':case'4':
			{
				dictionary.getAllDefinitions();
			}
			}
		}while(answer!='e' && answer!='5');
		System.out.println("Program terminat.");
		

	}

}
